import argparse
import sys
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
from .hardware import Camera

def main():
    parser = argparse.ArgumentParser(description="Xml RPC for hamamatsu betsa")
    parser.add_argument('--port',dest="port",type=int,default=8000)
    args = parser.parse_args()
    with SimpleXMLRPCServer(('', args.port)) as server:
        camera = Camera()
        server.register_instance(camera)
        print(f'Server started on port {args.port}')
        try:
            server.serve_forever()
        except KeyboardInterrupt:
            sys.exit(0)
