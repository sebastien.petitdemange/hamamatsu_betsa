import enum
import tabulate
import numpy as np
from matplotlib import pyplot as plt
import lmfit
from scipy.signal import find_peaks
import xmlrpc.client
from bliss.config.beacon_object import BeaconObject
from bliss.controllers.counter import CounterController
from bliss.common.counter import Counter
from bliss.scanning.chain import AcquisitionSlave

#NEON FUNCTION
def gaussian(x,peak,width):
    return 1/(width*np.sqrt(2*np.pi))*np.exp(-0.5*((x-peak)/width)**2)

def wavelength(pixels, offset, slope, q_corr):
    return slope*np.arange(len(pixels)) + offset + q_corr*np.arange(len(pixels))**2

def pixel2lambda(pixel, offset, slope, q_corr):
    return slope*pixel + offset + q_corr*pixel**2

def gaussian_peak(x,peak,width,hight):
    return hight*1/(width*np.sqrt(2*np.pi))*np.exp(-0.5*((x-peak)/width)**2)

def neon_model_func_v2(pixels, offset, slope, q_corr, gauss_params):
    wavelength = offset + slope*np.arange(len(pixels)) + q_corr*np.arange(len(pixels))**2
    intens = np.zeros(len(wavelength))
    for i in range(4):
        intens += gaussian_peak(np.arange(len(pixels)),gauss_params[2*i+i],gauss_params[2*i+i+1],gauss_params[2*i+i+2])
    return intens

# neon lines present in the spectrum with their intensities
neon = np.array([6929.46734, 7032.41314, 7245.16664, 7438.9])
neon_intens = np.array([300., 900.,  500.,  150.])
neon_intens = neon_intens/max(neon_intens)
idx = np.argsort(neon_intens)
sorted_neon = neon[idx[::-1]]

#RUBY Functions
def datchi2007(T):
    dT = T-296
    if T < 50:
        dLR = -.887
    if 50 <= T < 296 : 
        dLR = 0.00664*dT + 6.76e-6 *dT**2 - 2.33e-8 *dT**3
    if T >= 296 :
        dLR = 0.00746*dT - 3.01e-6*dT**2 + 8.76e-9*dT**3
    return dLR*10

def ruby_shen_2020(L0,LR,T):
    LR = LR-datchi2007(T)
    return 1.87e3*(LR-L0)/L0*(1+5.63*(LR-L0)/L0)


def ruby_dewaele_2008(L0,LR,T):
    # reference: Dewaele A., Torrent M., Loubeyre P. and Mezouar M. (2008) Phys. Rev. B 78, 104102, https://doi.org/10.1103/PhysRevB.78.104102
    LR = LR-datchi2007(T)
    A = 1920
    B = 9.61
    return A/B*((LR/L0)**B-1)

def ruby_mao_nh_1986(L0,LR,T):
    LR = LR-datchi2007(T)
    # reference: Mao H.K., Xu J., and Bell P.M. (1986) J. Geophys. Res. 91, 4673 
    A = 1904
    B = 5
    return A/B*((LR/L0)**B-1) 



# Sm pressure scales:

def samarium_1_trots_2013(LR):
    # reference: Trots D.M., Kurnosov A., Boffa Ballaran T., Tkachev S., Zhuravlev K., Prakapenka V., Berkowski. M and Frost D., JGR:Solid Earth, 118, 5805 (2013)
    L0 = 6178.2
    A = 2089.91
    B = -4.43
    return A/B*((LR/L0)**B-1) 

def samarium_2_trots_2013(LR):
    # reference: Trots D.M., Kurnosov A., Boffa Ballaran T., Tkachev S., Zhuravlev K., Prakapenka V., Berkowski. M and Frost D., JGR:Solid Earth, 118, 5805 (2013)
    L0 = 6160.7
    A = 2578.22
    B = -15.38
    return A/B*((LR/L0)**B-1)

def Sm_SrB4O7_2015(LR):
    # reference: S.V. Rashenko, A. Kurnosov, L. Dubrovinsy and K. D. Litasov, J. Appl. Phys. 117, 145902  
    L0 = 6853.87
    A = 2836.
    B = -14.3
    return A/B*((LR/L0)**B-1)

# pseudovoigt model
def ruby_model_func(x, amp, peak1, ratio, peak2, sigma, rpsv, background):
    sigmag = sigma/np.sqrt(2*np.log(2))
    psv1 = (1-rpsv)/(sigmag*np.sqrt(2*np.pi))*np.exp(-0.5*((x-peak1)/sigmag)**2) + rpsv/np.pi*(sigma/((x-peak1)**2 + sigma**2))
    psv2 = (1-rpsv)/(sigmag*np.sqrt(2*np.pi))*np.exp(-0.5*((x-peak2)/sigmag)**2) + rpsv/np.pi*(sigma/((x-peak2)**2 + sigma**2))

    return amp*(psv1 + ratio*psv2) + background

class Client(BeaconObject):
    class BACKGROUND_MODE(enum.Enum):
        OFF=0
        ON=1

    class CALIBRANT(enum.Enum):
        RUBY_SHEN_2020 = 0
        RUBY_DEWAELE_2008 = 1
        RUBY_MAO_NH_1986 = 2
        SAMARIUM_1_TROTS_2013 = 3
        SAMARIUM_2_TROTS_2013 = 4
        SM_SRB4O7_RASHENKO_2015 = 5
        
    exposure_time = BeaconObject.property_setting('exposure_time',default=0.4)
    background_mode = BeaconObject.property_setting('background_mode',default=BACKGROUND_MODE.OFF)
    calibrant_mode = BeaconObject.property_setting('calibrant_mode',default=CALIBRANT.RUBY_SHEN_2020)
    
    calibration_params = BeaconObject.property_setting('calibration_params',default=(6797.02319,0.6848,-4.4034e-5))
    lambda_0_ruby = BeaconObject.property_setting('lambda_0_ruby',default=6942.8)
    temperature = BeaconObject.property_setting('temperature', default=296)

    roi_range = BeaconObject.property_setting('roi_range',default=(0,1044))
    
    def __init__(self,name,config):
        BeaconObject.__init__(self,config,name,share_hardware=False)
        
        host = config['host']
        port = config.get('port',8000)
        self._camera = xmlrpc.client.ServerProxy(f"http://{host}:{port}/")

        self._counters_container = _PrlCounter(self)

    @property
    def counters(self):
        return self._counters_container.counters

    @background_mode.setter
    def background_mode(self,value):
        if isinstance(value,(bool,int)):
            return self.BACKGROUND_MODE.ON if value else self.BACKGROUND_MODE.OFF
        elif isinstance(value,str):
            value = value.upper()
            for v in self.BACKGROUND_MODE:
                if v.name == value:
                    return v
        return value
    
    @calibrant_mode.setter
    def calibrant_mode(self,value):
        if isinstance(value,(int)):
            for v in self.CALIBRANT:
                if v.value == value:
                    return v
        elif isinstance(value,str):
            for v in self.CALIBRANT:
                if v.name == value.upper():
                    return v
        elif isinstance(value,self.CALIBRANT):
            return value
        raise RuntimeError(f"Calibrant mode is {value}, possible values are:",["%d -> %s" % (x.value,x.name) for x in self.CALIBRANT])
            
    def raw_acquisition(self, time, background=False):
        if background:
            return self._camera.acquisition_with_background(time)
        else:
            return self._camera.acquisition(time)
        
    def read_spectrum(self):
        return self.raw_acquisition(self.exposure_time,self.background_mode)

    def calibration(self,expo_time=None):
        if expo_time is None:
            expo_time = self.exposure_time

        d = np.array(self.raw_acquisition(expo_time))

        # estimate offset and slope from first two strong peaks
        peaks, peaks_properties = find_peaks(d, height=50, width=5)
        idx = np.argsort(peaks_properties['peak_heights'])
        sorted_peaks = peaks[idx[::-1]]
        sorted_width = peaks_properties['widths'][idx[::-1]]
        first_two_strong_peaks= sorted_peaks[0:2]
        max1 = first_two_strong_peaks[1]
        l1 = sorted_neon[1]
        max2 = first_two_strong_peaks[0]
        l2 = sorted_neon[0]
        sl = (l1-l2)/(max1-max2)
        off = l1 -sl*max1
        print('Estimation from two strongest peaks : slope = ', sl, 'offset = ', off )

        
        # 1. Fit Gaussians to peaks
        gaussian_peak_model = lmfit.Model(gaussian_peak)
        popt0 = []
        for i in range(4):
            tmp = np.copy(d)/max(d)
            tmp[0:sorted_peaks[i]-15]=0
            tmp[sorted_peaks[i]+15:]=0
            par0 = lmfit.Parameters()
            par0.add_many(('peak', sorted_peaks[i], True, sorted_peaks[i]-10,sorted_peaks[i]+10),
                ('width',  sorted_width[i] , True, 0, 10, ),
                ('hight', 0.5, True, 0., 100.))
            res0 = gaussian_peak_model.fit(tmp, par0, x=np.arange(len(tmp)))
            for param in res0.params.values():
                popt0.append(param.value)
                # perr.append(param.stderr)
        fitted_peaks = [popt0[0],popt0[3],popt0[6],popt0[9]]
                        
        # 2. polynomial fit to peaks
        wavelength_model = lmfit.Model(pixel2lambda,independent_vars=['pixel'])
        par1 = lmfit.Parameters()
        par1.add_many(('offset',  off , True, 6700, 6900, ),
                        ('slope', sl , True, 0.6, 0.8, ),
                        ('q_corr', 0, True, -0.1, 0.1, ))
        res1 = wavelength_model.fit(sorted_neon, par1, pixel=fitted_peaks)
        #print(res1.fit_report())
        popt = []
        perr = []
        for param in res1.params.values():
            popt.append(param.value)
            perr.append(param.stderr)
        
        off =  popt[0]
        sl = popt[1]    
        q_corr = popt[2]

        
        if(any(x is None for x in popt)):
            print('Fit did not work')
            return
        else:        
             print('Fit resulst: offset  = ', np.round(popt[0],2), ' slope = ', np.round(popt[1],4),  ' q_corr = ', np.format_float_scientific(popt[2], precision=4))
        w =  wavelength(d, off, sl, q_corr)
        intens = neon_model_func_v2(d, off, sl, q_corr, popt0)

        plt.figure()
        plt.plot(w,d/max(d),label='data')
        plt.plot(w,intens,'--k', label= 'fit')
        plt.title('Neon Calibration')
        plt.xlabel('Wavelength [nm]')
        plt.ylabel('Intensity')
        plt.legend()
        plt.show(block=True)

        self.calibration_params =  off, sl, q_corr

    def __info__(self):
        all_info = [(x,y) for x,y in self.settings.items()]
        return tabulate.tabulate(all_info)

    
class _oneDCounter(Counter):
    def __init__(self,*args,**kargs):
        super().__init__(*args,**kargs)
        self._shape = 1044
    @property
    def shape(self):
        return (self._shape,)
    
class _PrlCounter(CounterController):
    def __init__(self,client):
        super().__init__(client.name,register_counters=True)

        for cnt_name in ['pressure','lambda','roi_sum']:
            Counter(cnt_name,self)

        self._spectrum = _oneDCounter('spectrum',self)
        self._spectrum_fit = _oneDCounter('spectrum_fit',self)
        self._wavelength = _oneDCounter('wavelength',self)
        self._client = client

    def get_acquisition_object(self,acq_params,ctrl_params,parent_acq_params):
        return _PrlAcquisitonSlave(self._client)

    def get_default_chain_parameters(self,*args):
        return {}


class _PrlAcquisitonSlave(AcquisitionSlave):
    def __init__(self,client,npoints=1):
        self._client = client
        super().__init__(client._counters_container,npoints=npoints,
                         trigger_type=AcquisitionSlave.SOFTWARE,
                         prepare_once=False,start_once=False)

    def prepare(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def trigger(self):
        spectrum = self._client.read_spectrum()

        pressure,lambda_val,wavelength,spectrum_fit = self._get_pressure_lamda_spectrumfit(spectrum)

        roi_range = self._client.roi_range
        roi_sum = sum(spectrum[roi_range[0]:roi_range[1]])

        #set new shape for counters
        prlcounters = self._client._counters_container
        spectrum_cnt = prlcounters._spectrum
        spectrum_cnt._shape = len(spectrum)

        spectrum_fit_cnt = prlcounters._spectrum_fit
        spectrum_fit_cnt._shape = len(spectrum_fit)

        wavelength_cnt = prlcounters._wavelength
        wavelength_cnt._shape = len(wavelength)

        return self.channels.update({'spectrum':spectrum,
                                     'pressure': pressure,
                                     'lambda':lambda_val,
                                     'spectrum_fit':spectrum_fit,
                                     'roi_sum':roi_sum,
                                     'wavelength':wavelength})

    def _get_pressure_lamda_spectrumfit(self,spectrum):
        # calibration parameters
        c0,c1,c2 = self._client.calibration_params
        roi_start,roi_end = self._client.roi_range
        y = np.array(spectrum)
        y2 = np.ndarray.copy(y)
        y2[0:roi_start]=0
        y2[roi_end:]=0
        x = wavelength(y, c0, c1, c2)
        
        LR_init = x[np.argmax(y2)]
        amp1_init = y2[np.argmax(y2)]
        LR2_init = x[np.argmax(y2)]-15
        index = np.argmin(np.abs(x-LR2_init))
        amp2_init = y2[index]
        background = y2[np.argmax(y2)-40]
        ratio = (amp2_init-background)/(amp1_init-background)
        ratio = amp2_init/amp1_init
        ruby_fit = lmfit.Model(ruby_model_func)
        params = lmfit.Parameters()
        try:
            params.add_many(('amp',  amp1_init*10 , True),
                        ('peak1', LR_init , True, LR_init*.85, LR_init*1.15 ),
                        ('ratio',  ratio , True, 0.6*ratio, 1.4*ratio),   
                        ('peak2', LR2_init , True, LR2_init*.85, LR2_init*1.15 ),
                        ('sigma',  4 , True),
                        ('rpsv', .7, True),
                        ('background', background, True)  )
        except ValueError:
            print("Data couldn't be fitted")
            return 0.,0.,x,np.zeros((len(x),))

        yfit = y[roi_start:roi_end]
        xfit = x[roi_start:roi_end]
        ruby_result = ruby_fit.fit(yfit, params, x = xfit)

        popt = []
        perr = []
        for param in ruby_result.params.values():
            popt.append(param.value)
            perr.append(param.stderr)

        LR = popt[1]
        L0 = self._client.lambda_0_ruby
        T = self._client.temperature
        if(self._client.calibrant_mode == Client.CALIBRANT.RUBY_SHEN_2020):
            pressure = ruby_shen_2020(L0,LR,T)
        elif(self._client.calibrant_mode == Client.CALIBRANT.RUBY_DEWAELE_2008):
            pressure = ruby_dewaele_2008(L0,LR,T)
        elif(self._client.calibrant_mode == Client.CALIBRANT.RUBY_MAO_NH_1986):
            pressure = ruby_mao_nh_1986(L0,LR,T)
        elif(self._client.calibrant_mode == Client.CALIBRANT.SAMARIUM_1_TROTS_2013):
            pressure = samarium_1_trots_2013(LR)
        elif(self._client.calibrant_mode == Client.CALIBRANT.SAMARIUM_2_TROTS_2013):
            pressure = samarium_2_trots_2013(LR)
        elif(self._client.calibrant_mode == Client.CALIBRANT.SM_SRB4O7_RASHENKO_2015):
            pressure =  Sm_SrB4O7_2015(LR)
        else:
            raise RuntimeError("Calibrant not managed yet")
        spectrum_fit = ruby_model_func(x,*popt)
        return pressure,LR,x,spectrum_fit

