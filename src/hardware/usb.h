#include <iostream>
#include <libusb-1.0/libusb.h> /* libusb header */

class RawCam
{
 public:
  RawCam();
  ~RawCam();

  void read_spectrum(unsigned short spectrum[],int& size,bool shutter_open=false);
  void open_shutter();
  void reset();
  
  void raw_write(int cmd);
  void raw_read(unsigned char data[],int& size);
private:
  libusb_context*	m_ctx;
  libusb_device_handle*	m_handle;
};
