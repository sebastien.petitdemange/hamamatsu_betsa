# distutils: language = c++
from libcpp import bool

cdef extern from "usb.h":
    cdef cppclass RawCam:
        RawCam() except +
        void read_spectrum(unsigned short spectrum[],int& size) except +
        void read_spectrum(unsigned short spectrum[],int& size,bool) except +
        void open_shutter() except +
        void reset() except +
        void raw_write(int cmd) except +
        void raw_read(unsigned char data[],int& size) except +
