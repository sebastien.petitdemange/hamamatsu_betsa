#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "usb.h"

#define MICROCHIP_ID 0x04d8
#define PRODUCT_ID 0x000c


const static unsigned char endpoint_in=0x81; /* endpoint 0x81 address for IN */
const static unsigned char endpoint_out=1; /* endpoint 1 address for OUT */

// Commands
const static unsigned char START_SCAN = 0x04;
const static unsigned char READ_NEXT_PACKET = 0x08;
const static unsigned char SHUTTER_OPEN = 0x02;
const static unsigned char RESET = 0xfd;

RawCam::RawCam() : m_ctx(NULL),m_handle(NULL)
{
  int return_flag = libusb_init(&m_ctx);
  if(return_flag < 0)
    throw std::runtime_error("Can not initialize libusb");

  libusb_set_option(m_ctx,LIBUSB_OPTION_LOG_LEVEL,3);

  m_handle = libusb_open_device_with_vid_pid(m_ctx, MICROCHIP_ID, PRODUCT_ID);
  if(!m_handle)
    {
      //Check if we can see the device on the USB bus
      bool found = false;
      libusb_device **devs;
      int cnt = libusb_get_device_list(m_ctx, &devs);
      for(int i = 0;!found && i < cnt;++i)
	{
	  libusb_device *dev = devs[i];
	  libusb_device_descriptor desc;
	  int r = libusb_get_device_descriptor(dev, &desc);
	  if(r < 0) continue;
	      
	  found = (desc.idVendor == MICROCHIP_ID &&
		   desc.idProduct == PRODUCT_ID);
	}
      libusb_free_device_list(devs, 1);
      
      if(found)
	throw std::runtime_error("Device is found on the Bus but cannot be open "
				 "(Check the permission)");
      else
	throw std::runtime_error("Device not found is it plugged");
    }
}

RawCam::~RawCam()
{
  if(m_ctx)
    libusb_exit(m_ctx);
  if(m_handle)
    libusb_close(m_handle);
}

void RawCam::read_spectrum(unsigned short spectrum[],int &size,bool shutter_open)
{
  int spectrum_size = size;
  int spectrum_index = 0;
  unsigned char buffer[64];
  int buffer_size;

  unsigned char start_scan = shutter_open ? START_SCAN|SHUTTER_OPEN : START_SCAN;
  
  raw_write(start_scan);
  buffer_size = sizeof(buffer);
  raw_read(buffer,buffer_size);
  //should reply command echo i.e 0x04
  if(buffer_size != 1)
    throw std::runtime_error("Wrong reply size");
  if(buffer[0] != start_scan)
    throw std::runtime_error("This answer is not expected");

  unsigned char read_next_packet = shutter_open ? READ_NEXT_PACKET|SHUTTER_OPEN : READ_NEXT_PACKET;
  usleep(50000);
  while(spectrum_index < spectrum_size)
    {
      raw_write(read_next_packet);
      buffer_size = 64;
      raw_read(buffer,buffer_size);
      if(buffer_size != 64)
	throw std::runtime_error("Wrong reply size during spectrum read");
      unsigned char nb_bytes = buffer[0];
      if(!nb_bytes) break;
      
      unsigned short* pt = ((unsigned short*)buffer) + 1;
      for(int i = 0;i < (nb_bytes / 2) && spectrum_index < spectrum_size;++i)
	spectrum[spectrum_index++] = pt[i];
    }
  size=spectrum_index;
}
void RawCam::open_shutter()
{
  unsigned char reply[1];
  int size = sizeof(reply);
  raw_write(SHUTTER_OPEN);
  raw_read(reply,size);
  if(!size)
    throw std::runtime_error("open_shutter: should reply 1 byte");
  if(reply[0] != SHUTTER_OPEN)
    throw std::runtime_error("open_shutter: didn't expect this reply");
}

void RawCam::reset()
{
  unsigned char reply[64];
  int size = sizeof(reply);
  raw_write(RESET);
  raw_read(reply,size);
}

void RawCam::raw_write(int cmd)
{
  unsigned char msg[] = {(unsigned char)(cmd)};

  int ret = libusb_interrupt_transfer(m_handle,endpoint_out,msg,
				      1,NULL,100);
  if(ret)
    {
      switch(ret)
	{
	case LIBUSB_ERROR_TIMEOUT: std::runtime_error("raw_write timeout");break;
	case LIBUSB_ERROR_NO_DEVICE: std::runtime_error("raw_write no device");break;
	case LIBUSB_ERROR_PIPE: std::runtime_error("raw_write endpoint halted");break;
	default: std::runtime_error("raw_write unknown error");break;
	}
    }
}

void RawCam::raw_read(unsigned char data[],int& size)
{
  int transferred;
  int ret = libusb_interrupt_transfer(m_handle,endpoint_in,data,size,&transferred,1000);
  if(ret)
    {
      switch(ret)
	{
	case LIBUSB_ERROR_TIMEOUT: throw std::runtime_error("raw_read timeout");break;
	case LIBUSB_ERROR_NO_DEVICE: throw std::runtime_error("raw_read no device");break;
	case LIBUSB_ERROR_PIPE: throw std::runtime_error("raw_read endpoint halted");break;
	default: throw std::runtime_error("raw_read unknown error");break;
	}
    }
   size = transferred;
}
