# distutils: language = c++   
import time
from camera cimport RawCam

cdef class Camera:
    cdef RawCam* c_cam

    def __init__(self):
        self.c_cam = new RawCam()

    def acquisition(self,double acq_time):
        cdef unsigned short spectrum[1044]
        cdef int size = 1044;
        #flush the current spectrum
        for i in range(2):
            self.c_cam.read_spectrum(spectrum,size)
        if not size:
            raise RuntimeError("Camera not connected")
        #take the spectrum
        self.c_cam.open_shutter()
        time.sleep(acq_time)
        size = 1044
        self.c_cam.read_spectrum(spectrum,size,True)
        self.c_cam.reset()
        return spectrum
    
    def acquisition_with_background(self,double acq_time):
        cdef unsigned short spectrum[1044]
        cdef unsigned short background[1044]
        cdef int size = 1044;
        #flush the current spectrum
        for i in range(2):
            self.c_cam.read_spectrum(spectrum,size)
        if not size:
            raise RuntimeError("Camera not connected")
        #take the background spectrum
        size = 1044
        self.c_cam.read_spectrum(background,size)
        #open the shutter
        self.c_cam.open_shutter()
        time.sleep(acq_time)
        size = 1044
        self.c_cam.read_spectrum(spectrum,size,True)
        self.c_cam.reset()

        for i in range(size):
            if background[i] > spectrum[i]:
                spectrum[i] = 0
            else:
                spectrum[i] -= background[i]
        return spectrum
        
    def read_spectrum(self):
        cdef unsigned short spectrum[1044]
        cdef int size = 1044;
        self.c_cam.read_spectrum(spectrum,size)
        if not size:
            raise RuntimeError("Camera not connected")
        return spectrum
    
    def raw_write(self,int cmd):
        self.c_cam.raw_write(cmd)

    def raw_read(self):
        cdef unsigned char buffer[64]
        cdef int size=64
        self.c_cam.raw_read(buffer,size)
        return buffer[:size]
