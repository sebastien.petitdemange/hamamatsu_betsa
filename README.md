# Hamamatsu Betsa

This module control Hamamatsu C7041 through Betsa boxes

# Install

This module should be installed on the server pc and on the client.

On server pc (linux) you should copy **udev/99-betsa.rules** on **/etc/udev/rules.d**.

Need also **libusb** i.e (mamba install libusb)

