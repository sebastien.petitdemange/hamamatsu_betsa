from setuptools.extension import Extension
from setuptools import setup


camera = Extension("hamamatsu_betsa.hardware",
                   sources=["src/hardware/usb.cpp","src/hardware/camera.pyx"],
                   language="c++",
                   libraries=['usb-1.0'])
def main():
    setup(name='hamamatsu betsa',
          author='seb',
          version='1.0.0',
          description='Hamamatsu C7041 control through Betsa boxes',
          package_dir={"hamamatsu_betsa": "hamamatsu_betsa"},
          packages=["hamamatsu_betsa"],
          ext_modules=[camera],
          install_requires=['cython'],
          entry_points={"console_scripts":["hamamatsu_betsa = hamamatsu_betsa.server:main"]},
          )


if __name__ == "__main__":
    main()
      
